/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneradorDOM;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author lliurex
 */
public class GeneradorDOM {
    public Document document;
    
    //constructor por defecto
    public GeneradorDOM() throws ParserConfigurationException{
        DocumentBuilderFactory factoria = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factoria.newDocumentBuilder();
        
        //creo el nuevo documento
        document = builder.newDocument();
        }//fin constructor
    
    public void GenerarDocument(){
        Element productos = document.createElement("productos");
        document.appendChild(productos);
        
        Element producto = document.createElement("producto");
        productos.appendChild(producto);
        
        producto.setAttribute("codigo", "1"); //Atributo XML
        
        //añado hijos a productos
        Element nombre = document.createElement("nombre");
        nombre.appendChild(document.createTextNode("teclado")); // de tipo texto
        producto.appendChild(nombre);
        
        Element descripcion = document.createElement("descripcion");
        descripcion.appendChild(document.createTextNode("El teclado Logitech® G15 ofrece el equipo y "
        + "la información que necesita para ganar.")); // de tipo texto
        producto.appendChild(descripcion);
        
        Element ubicacion = document.createElement("ubicacion");
        ubicacion.appendChild(document.createTextNode("Estanteria A14, Balda 2")); // de tipo texto
        producto.appendChild(ubicacion);
        
        
        
        
        
       
    } //fin generarDocument
    
    public void generarXml() throws TransformerConfigurationException, IOException, TransformerException {
        TransformerFactory factoria = TransformerFactory.newInstance();
        Transformer transformer = factoria.newTransformer();
        
        Source source = new DOMSource (document); //documento origen datos
        File file = new File ("C:\\AaD\\ejemplo.xml");
        FileWriter fw = new FileWriter (file); //abro para escritura
        PrintWriter pw = new PrintWriter (fw); //para poder escribir
        Result result = new StreamResult (pw);
        transformer.transform(source, result); //source = document
                                                //result = fich xml  
    }

}
